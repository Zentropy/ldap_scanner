Start with

nohup ./ldap_scanner &

You can set it to autostart on debian derivatives via:

> sudo touch /etc/profile.d/ldap_scanner.sh\
> Into that file:\
>    #!/bin/bash\
>    nohup /path/to/ldap_scanner &\
\
> sudo chmod +x /etc/profile.d/ldap_scanner.sh