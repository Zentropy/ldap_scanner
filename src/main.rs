extern crate ldap3;
extern crate sendgrid;
extern crate log;
extern crate log4rs;

use dotenv;
use ldap3::{LdapConn, Scope, LdapConnSettings};
use sendgrid::v3::*;
use log::{info, error};
use std::time::Duration;
use std::thread;
use clokwerk::{Scheduler, TimeUnits};

//fucking mimalloc compiles without cmake flags turning off the huge debug dump during run
#[cfg(not(debug_assertions))]
use mimalloc::MiMalloc;

#[cfg(not(debug_assertions))]
#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

fn main() {

    dotenv::dotenv().ok();

    log4rs::init_file("LoggerConfig.yaml", Default::default()).unwrap();
    info!("Beginning test loop");

    let ldap_uri = dotenv::var("LDAP_URI").unwrap();
    let ldap_user = dotenv::var("LDAP_USER").unwrap();
    let ldap_pass = dotenv::var("LDAP_PASS").unwrap();

    let mut scheduler = Scheduler::new();

    scheduler.every(30.minutes()).run(move || {
        if let Err(e) = test_connection(&ldap_uri, &ldap_user, &ldap_pass) {
            could_not_connect(&e);
        }
    });

    loop {
        scheduler.run_pending();
        //Sleep just over 30 minutes/1800 seconds so each run_pending triggers the test for sure
        thread::sleep(Duration::from_secs(1810));
    }
}

fn test_connection(ldap_uri: &String, ldap_user: &String, ldap_pass: &String) -> Result<(), String> {

    let ldapsettings = LdapConnSettings::new()
        .set_no_tls_verify(true)
        .set_conn_timeout(Duration::new(1, 0));

    let ldap = LdapConn::with_settings(ldapsettings, ldap_uri.as_str())
        .map_err(|e| e.to_string())?;

    ldap.simple_bind(ldap_user.as_str(), ldap_pass.as_str())
        .map_err(|e| e.to_string())?;

    ldap.search("DC=ptc,DC=tec,DC=ar,DC=us",
                Scope::Subtree,
                "(objectClass=userPrincipalName)",
                vec!["l"])
        .map_err(|e| e.to_string())?;

    info!("Successful Connection.");
    Ok(())
}

fn could_not_connect(e: &String) {
    error!("COULD NOT CONNECT: {:?}", e);

    let p = Personalization::new()
        .add_to(Email::new().set_email(dotenv::var("TO_EMAIL").unwrap().as_str()));

    let m = Message::new()
        .set_from(Email::new()
            .set_email(dotenv::var("FROM_EMAIL").unwrap().as_str())
            .set_name(dotenv::var("FROM_NAME").unwrap().as_str()))
        .set_subject(dotenv::var("EMAIL_SUBJECT").unwrap().as_str())
        .add_content(
            Content::new()
                .set_content_type("text/html")
                .set_value(dotenv::var("EMAIL_TEXT").unwrap().as_str()))
        .add_personalization(p);


    let sender = Sender::new(dotenv::var("SENDGRID_API_KEY").unwrap());

    if let Err(e) = sender.send(&m) {
        error!("Unable to send email: {:?}", e);
    }
}

//////////////////////////TESTING

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reports_successful_connection() {
        let mut ldap_uri = dotenv::var("LDAP_URI").unwrap();
        let ldap_user = dotenv::var("LDAP_USER").unwrap();
        let ldap_pass = dotenv::var("LDAP_PASS").unwrap();

        assert_eq!(test_connection(&ldap_uri, &ldap_user, &ldap_pass), Ok(()));
    }

    #[test]
    fn reports_unsuccessful_connection() {
        let mut ldap_uri = dotenv::var("LDAP_URI").unwrap();
        let ldap_user = dotenv::var("LDAP_USER").unwrap();
        let ldap_pass = dotenv::var("LDAP_PASS").unwrap();

        //change ldap port to obviously non-working one so connection fails
        ldap_uri.push_str("99");
        assert_eq!(test_connection(&ldap_uri, &ldap_user, &ldap_pass), Err("timeout".to_string()));
    }
}